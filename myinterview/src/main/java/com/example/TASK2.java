package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 
{
	private String TAG = TASK2.class.getName();
	private List<Integer> list = new ArrayList<Integer>();
	
	public TASK2() 
	{
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.add(8);
		list.add(9);
		list.add(10);
	}
	
	public void removeMiddleElement()
	{
		int middle = list.size() / 2;
		int element = list.get(middle);
		
		System.out.println(TAG + " List before removed: " + list);

		list.remove(middle);
		
		System.out.println(TAG + " Element removed: " + element);
		System.out.println(TAG + " List after removed: " + list);
	}
}
