package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 
{
    public static final String LUKE = "Luke";
    public static final String YODA = "Yoda";
    public static final String OBIWAN = "Obi-Wan";
    public static final String ANAKIN = "Anakin";
    
	private String TAG = TASK3.class.getName();
	
	private List<String> jedi = new ArrayList<String>();
	
	public TASK3() 
	{
		jedi.add(LUKE);
		jedi.add(YODA);
		jedi.add(OBIWAN);
		jedi.add(ANAKIN);
		jedi.add(YODA);
		jedi.add(LUKE);
		jedi.add(OBIWAN);
		jedi.add(LUKE);
		jedi.add(YODA);
		jedi.add(OBIWAN);
	}
	
	public void countDistinctsItems()
	{		
		HashSet<String> hash = new HashSet<String>();
		hash.addAll(jedi);
		
		System.out.println(TAG + " Total items on list: " + jedi.size() + ". Distincts items number: " + hash.size());
	}
}
