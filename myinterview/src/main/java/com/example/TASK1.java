package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */

public class TASK1 
{	
	private String TAG = TASK1.class.getName();
	private String word;
	
	public TASK1(String word) 
	{
		this.word = word;
	}
	
	public void isStringPalindrome()
	{
		String reverse = new StringBuilder(word).reverse().toString();

		if(reverse.equals(word))
		{
			System.out.println(TAG + " Is a palindrome!");
		}
		else
		{
			System.out.println(TAG + " Isn't a palindrome");	
		}
	}
}
